script ArduinoAggressionExample
 shows an example of using the Arduino timer in order to detect squeeze in
 first sensor, second sensor or both.

 INSTRUCTIONS:
 Before running for the first time (Windows): 
 1. download and install the support package for arduino from
 https://www.mathworks.com/hardware-support/arduino-matlab.html. No servers
 are necessary.
 2. Check which port the sensors are connected to using the device
 manager->Ports(COM & LPT)->USB Serial Port
 3. Change 'COM7' port below to your port.

 Date: March 18, 2019
 Version: 1.0
 Author: Danielle Honigstein and Eva Mishor, Noam Sobel's Olfaction Lab, Weizmann
 Institute of Science
 Contact: eva.mishor@weizmann.ac.il, danielle.honigstein@weizmann.ac.il

 If you find this code useful please credit us in any papers/software
 published
